# nativescript-pointzi

## Prerequisites / Requirements

Head on over to https://dashboard.pointzi.com/ and sign up for an account.

## Installation

Follow the instructions on https://dashboard.pointzi.com/setting to install Pointzi on your NativeScript app.

## License

Apache License Version 2.0, January 2004
