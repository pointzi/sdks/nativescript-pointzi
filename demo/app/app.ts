import * as app from "tns-core-modules/application";
import { BottomNavigation } from "tns-core-modules/ui/bottom-navigation"

import { handleOpenURL, AppURL } from 'nativescript-urlhandler';

handleOpenURL((appURL: AppURL) => {
    var page = appURL.toString().replace("demo://", "")
    var nav: BottomNavigation = app.getRootView().getViewById("nav")

    switch (page) {
        case "home":
            nav.selectedIndex = 0
            break;
        case "browse":
            nav.selectedIndex = 1
            break;
        case "search":
            nav.selectedIndex = 2
            break;
    }
});

app.run({ moduleName: "app-root" });

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
