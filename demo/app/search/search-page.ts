import { Pointzi } from "nativescript-pointzi";
import { NavigatedData, Page } from "tns-core-modules/ui/page";
import { SearchViewModel } from "./search-view-model";

export function onNavigatingTo(args: NavigatedData) {
    const page = <Page>args.object;
    page.bindingContext = new SearchViewModel();
}

export function onNavigatedTo(args: NavigatedData ) {
    new Pointzi().setViewName("search");
}
