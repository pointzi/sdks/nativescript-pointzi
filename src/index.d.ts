import { Common } from './pointzi.common';
export declare class Pointzi extends Common {
  setUserId(id: string);
  setUserId(id: string);
  tagString(key: string, tag: string);
  tagNumeric(key: string, numericValue: number);
  tagDatetime(key: string, date: string);
  incrementTag(key: string);
  incrementTagBy(key: string, value: number);
  removeTag(key: string);
  tagUserLanguage(language: string);
  showGuide(id: string);
  setViewName(view: string);
}
