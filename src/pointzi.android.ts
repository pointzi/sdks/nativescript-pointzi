import { Common } from './pointzi.common';
import * as application from "tns-core-modules/application";

export class Pointzi extends Common {
    setUserId(id: string) {
        com.pointzi.Pointzi.setUserId(id);
    }

    tagString(key: string, tag: string) {
        com.pointzi.Pointzi.tagString(key, tag);
    }

    tagNumeric(key: string, numericValue: number) {
        com.pointzi.Pointzi.tagNumeric(key, numericValue);
    }

    /*
      Date format in YYYY-MM-DD
      https://streethawk.freshdesk.com/solution/articles/5000672617-analytics
    */
    tagDatetime(key: string, date: string) {
        com.pointzi.Pointzi.tagDatetime(key, date);
    }

    incrementTag(key: string) {
        com.pointzi.Pointzi.incrementTag(key);
    }

    incrementTagBy(key: string, value: number) {
        com.pointzi.Pointzi.incrementTag(key, value);
    }

    removeTag(key: string) {
        com.pointzi.Pointzi.removeTag(key);
    }

    tagUserLanguage(language: string) {
        com.pointzi.core.StreetHawk.Tagger.INSTANCE.tagUserLanguage(language);
    }

    showGuide(id: string) {
        com.pointzi.Pointzi.showOnDemandCampaign(id);
    }

    setViewName(view: string) {
        console.log(view);
        (new com.pointzi.Player()).setReactViewActivity(view, com.pointzi.Player.getActivity());
    }

}
